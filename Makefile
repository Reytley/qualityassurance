.PHONY: hello clean phplint phpcompat

hello:
	@echo "Hello"

clean:
	@rm -Rf build

phplint.lst:
	@echo "fichier phplint.lst manquant."
	@false

build/phplint.txt: phplint.lst
	@test -d build || mkdir -p build
	@parallel-lint $$(cat phplint.lst) | tee $@

build/phpcompat.json:
	@test -d build || mkdir -p build
	@phpcs --report-json=$@.tmp -p || true
	@jq '.files' < $@.tmp | jq -r -f /scripts/phpcompat.jq > $@
	@rm $@.tmp

build/phpstan.json:
	@test -d build || mkdir -p build
	@phpstan --memory-limit=-1 --error-format=json > $@ || true
	@echo $$(jq '.totals.errors + .totals.file_errors' < build/phpstan.json)" erreur(s)."

build/outdated.json:
	@test -d build || mkdir -p build
	@composer outdated --format=json > $@.tmp || true
	@cat $@.tmp | jq -f /scripts/outdated.jq > $@
	@rm $@.tmp

build/audit.json:
	@test -d build || mkdir -p build
	@composer audit --format=json > $@.tmp || true
	@cat $@.tmp | jq -f /scripts/audit.jq > $@
	@rm $@.tmp


phpstan:
	@test -d build || mkdir -p build
	phpstan --memory-limit=-1


phplint: build/phplint.txt

phpcompat: build/phpcompat.json



outdated: build/outdated.json

audit: build/audit.json