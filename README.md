# Guidelines Hipay
Table des matières
===========
- [1. 💬 Contexte](#1-contexte)
- [2. 💡 Pré-requis](#2-pré-requis)
- [3. 🔧 Installation](#3-installation)
- [4. 💄 Qualité de code](#4-qualité-de-code)


## 1. 💬 Contexte
Quality assurance est un projet pour centraliser 
les différents outils d'assurance qualité de nos projets dans une application unique.
De plus il permet d'interfacer des outils PHP avec Sonar pour augmenter sont périmètre 

#### Les outils intégré dans ce projet sont :

#### PHPSTAN
Détecte des classes entières de bug avant même l'écriture de tests.
#### Lint
Lint se concentre sur la recherche systématique d'erreurs de syntaxe PHP dans le code.
l'outil derrière est un wrapper écrit en PHP qui exécute en multi-thread la commande php -l sur chaque fichier.
#### Compatibility
Il s'agit d'un ensemble de sniffs pour PHP CodeSniffer qui vérifie la compatibilité entre les versions de PHP.
Il vous permettra d'analyser votre code pour la compatibilité avec les versions supérieures et inférieures de PHP.
#### Audit

Il vous permettra d'analyser votre code pour la compatibilité avec les versions supérieures et inférieures de PHP.
#### Outdated
affiche une liste des packages installés pour lesquels des mises à jour sont disponibles,
y compris leurs versions actuelles et les plus récentes.

## 2. 💡 Pré-requis
_Ce qui est nécessaire d’avoir sur le poste du développeur_ :
- _Softs nécessaires (Docker, Docker-Compose)


### Chaque outil à besoin de sa configuration dans vos projets (exemple fourni):
#### - phpcs.xml.dist
#### - phplint.lst
#### - phpstan.neon.dist

Si Sonar est configuré dans votre projet il  faudra ajouter le lien vers les rapport et avoir les composer.lock/.json 
#### - phpstan.neon.dist
```
sonar.sources= ... ,composer.lock,composer.json
 ...
#COVERAGE
sonar.externalIssuesReportPaths=build/outdated.json,build/audit.json,build/phpcompat.json
sonar.php.phpstan.reportPaths=build/phpstan.json
 ...
```

### Exemple gitlab-ci.yml
``` 
php-audit:
  stage: analysis
  image: eu.gcr.io/ci-prod-262308/galopin/quality-assurance:latest
  script:
    -  make -f /Makefile audit
  artifacts:
    paths:
      - build/
    expire_in: 50 min
    
    
php-outdated:
  stage: analysis
  image: eu.gcr.io/ci-prod-262308/galopin/quality-assurance:latest
  before_script:
    - *init_ssh_agent
  script:
    -  make -f /Makefile outdated
  artifacts:
    paths:
      - build/
    expire_in: 50 min

```

## 3. 🔧 Installation
_Commandes à lancer pour installer le projet, suivant le projet_ :

1. commencer par `cd QualityAssurance`
2. builder l'image en lui donnant un nom sympa. Ex: `docker build -t galopin/quality-assurance:latest .`
3. placer le fichier `phpcs.xml.dist` à la racine d'un projet
4. éditer la liste des répertoires, le range de version php de ce fichier
5. lancer l'analyse `docker run --rm -w $(pwd) -v $(pwd):$(pwd) galopin/quality-assurance Audit`
6. un résumé est fourni dans la sortie standard (ça apparaitra dans les logs d'un job ci)
7. un fichier/rapport sera créé dans le dossier `build/` avec le détail

accessoirement, ajouter au `.gitignore` du projet :
```gitignore
/build/
```

## 4. 💄 Qualité de code

## PHPStan - PHP Static Analysis Tool
```bash
docker run --rm -v $(pwd):$(pwd) -w $(pwd) galopin/quality-assurance phpstan
```
## Lint
```bash
docker run --rm -v $(pwd):$(pwd) -w $(pwd) galopin/quality-assurance phplint
```
## Compatibility
```bash
docker run --rm -v $(pwd):$(pwd) -w $(pwd) galopin/quality-assurance phpcompat
```
## Audit
```bash
docker run --rm -v $(pwd):$(pwd) -w $(pwd) galopin/quality-assurance audit
```
## Outdated
```bash
docker run --rm -v $(pwd):$(pwd) -w $(pwd) galopin/quality-assurance outdated
```