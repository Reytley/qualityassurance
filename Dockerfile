FROM composer:2.4 as composer
FROM php:7.4-cli-alpine AS base

COPY --from=composer /usr/bin/composer /usr/bin/composer

# hadolint ignore=DL3018,SC2086
RUN apk --no-cache update \
    && apk --no-cache add openssh=~9.0 git=~2.36 unzip=~6.0 make=~4.3 jq=~1.6 \
    && apk --no-cache add --virtual .php-deps \
        libzip-dev=~1.8 \
        icu-dev=~71 \
    && apk --no-cache add --virtual .build-deps \
        $PHPIZE_DEPS \
    && docker-php-ext-install intl zip \
    && apk del .build-deps \
    && mkdir -m 700 /root/.ssh \
    && ssh-keyscan gitlab.groupeonepoint.com > /root/.ssh/known_hosts \
    && composer config --no-interaction --global allow-plugins.dealerdirect/phpcodesniffer-composer-installer true \
    && composer config --global gitlab-domains gitlab.groupeonepoint.com \
    && curl -s -o /root/.composer/keys.dev.pub https://composer.github.io/snapshots.pub \
    && curl -s -o /root/.composer/keys.tags.pub https://composer.github.io/releases.pub \
    && composer global require \
		"squizlabs/php_codesniffer=3.6.*" \
		"phpcompatibility/php-compatibility=^9.3" \
		"dealerdirect/phpcodesniffer-composer-installer=^0.7" \
    	"php-parallel-lint/php-parallel-lint=^1.3" \
    	"phpstan/phpstan=^1.7" \
    	"phpmd/phpmd=^2.12" \
        "rector/rector=^0.13.3"

COPY Makefile /Makefile
COPY scripts /scripts

ENV COMPOSER_ALLOW_SUPERUSER=1
ENV	COMPOSER_MEMORY_LIMIT=-1
ENV	COMPOSER_AUTH="{\"github-oauth\": {\"github.com\": \"8066a9dc613868dbd5a2a3dd79db8d76837cb458\"}}"
ENV	PATH="/root/.composer/vendor/bin:${PATH}"

CMD [ "/bin/ash"]


